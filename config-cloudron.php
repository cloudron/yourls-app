<?php

/* Cloudron LDAP Configuration */

if (!getenv('CLOUDRON_LDAP_SERVER')) return;

// LDAP configuration
define( 'LDAPAUTH_HOST', getenv('CLOUDRON_LDAP_SERVER') );
define( 'LDAPAUTH_PORT', getenv('CLOUDRON_LDAP_PORT') ); // LDAP server port - often 389 or 636 for TLS (LDAPS)
define( 'LDAPAUTH_BASE', getenv('CLOUDRON_LDAP_USERS_BASE_DN') ); // Base DN (location of users)
define( 'LDAPAUTH_USERNAME_FIELD', 'username'); // (optional) LDAP field name in which username is store
define( 'LDAPAUTH_SEARCH_USER', getenv('CLOUDRON_LDAP_BIND_DN') ); // (optional) Privileged user to search with
define( 'LDAPAUTH_SEARCH_PASS', getenv('CLOUDRON_LDAP_BIND_PASSWORD') ); // (optional) (only if LDAPAUTH_SEARCH_USER set) Privileged user pass
define( 'LDAPAUTH_BIND_WITH_USER_TEMPLATE', 'cn=%s,' . getenv('CLOUDRON_LDAP_USERS_BASE_DN') ); // (optional) Use %s as the place holder for the current user name
// without the cache or the ADD_NEW option cookies won't work and thus login won't work! See https://github.com/k3a/yourls-ldap-plugin/issues/4 and https://github.com/k3a/yourls-ldap-plugin/issues/8
define( 'LDAPAUTH_USERCACHE_TYPE', 1); // caches users in the options table.
define( 'LDAPAUTH_ADD_NEW', false ); // Add LDAP users to config.php
define( 'LDAPAUTH_ALL_USERS_ADMIN', false );

// $ldapauth_authorized_admins = array ();

