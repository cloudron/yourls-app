#!/bin/bash

set -eu

mkdir -p /run/yourls/session /app/data/user/plugins /app/data/user/languages

function setup() {
    while [[ ! -f "/var/run/apache2/apache2.pid" ]]; do
        echo "==> Waiting for apache2 to start"
        sleep 1
    done
 
    # this will create the initial .htaccess as well
    echo "==> Running first time setup"
    if ! curl --fail -v 'http://localhost:8000/admin/install.php?' --data 'install=Install+YOURLS'; then
        echo "==> Failed to setup"
        return 1
    fi

    if [[ -n "${CLOUDRON_LDAP_SERVER:-}" ]]; then
        echo "==> Activating LDAP plugin"
        mysql -u"${CLOUDRON_MYSQL_USERNAME}" -p"${CLOUDRON_MYSQL_PASSWORD}" -h mysql --database="${CLOUDRON_MYSQL_DATABASE}" \
            -e "UPDATE yourls_options SET option_value='a:1:{i:0;s:29:\"yourls-ldap-plugin/plugin.php\";}' WHERE option_name='active_plugins'"
    fi

    echo "==> Setup complete"
}

chown -R www-data.www-data /app/data /run/yourls

if [[ ! -f /app/data/user/config.php ]]; then
    echo "==> Detected first run"

    if [[ -n "${CLOUDRON_LDAP_SERVER:-}" ]]; then
        sed -e "s/##COOKIEKEY/$(pwgen -1s 32)/" \
            -e "/##DEFAULT_ADMIN/d" \
            /app/pkg/config.php > /app/data/user/config.php
    else
        sed -e "s/##COOKIEKEY/$(pwgen -1s 32)/" \
            -e "s/##DEFAULT_ADMIN/'admin' => 'changeme'/" \
            /app/pkg/config.php > /app/data/user/config.php
    fi

    chown www-data.www-data /app/data/user/config.php

    sudo -E -u www-data cp /app/pkg/index.html /app/data/index.html

    APACHE_CONFDIR="" source /etc/apache2/envvars
    rm -f "${APACHE_PID_FILE}"
    setup &
fi

echo "==> Ensure user directories"
for dir in `find "/app/code/user.orig"/* -maxdepth 0 -type d -printf "%f\n"`; do
    mkdir -p "/app/data/user/${dir}"
done

echo "==> Symlinking plugins"
for plugin in `find "/app/code/user.orig/plugins"/* -maxdepth 0 -type d -printf "%f\n"`; do
    rm -rf "/app/data/user/plugins/${plugin}"
    ln -sf "/app/code/user.orig/plugins/${plugin}" "/app/data/user/plugins/${plugin}"
done

# this is activated on startup
if [[ -n "${CLOUDRON_LDAP_SERVER:-}" ]]; then
    rm -f /app/data/user/plugins/yourls-ldap-plugin
    ln -sf /app/pkg/yourls-ldap-plugin /app/data/user/plugins/yourls-ldap-plugin
fi

echo "==> Upgrading"
cd /app/code && sudo -E -u www-data php /app/pkg/upgrade.php

echo "==> Starting yourls"
APACHE_CONFDIR="" source /etc/apache2/envvars
rm -f "${APACHE_PID_FILE}"
exec /usr/sbin/apache2 -DFOREGROUND

