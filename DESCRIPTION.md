YOURLS stands for Your Own URL Shortener. It is a small set of PHP scripts that will allow you to run your
own URL shortening service (a la TinyURL or Bitly).

Running your own URL shortener is fun, geeky and useful: you own your data and don't depend on third-party services.
It's also a great way to add branding to your short URLs, instead of using the same public URL shortener everyone uses.

## Features

* Free and open-source software.
* Private (your links only) or Public (everybody can create short links, fine for an intranet)
* Sequential or custom URL keywords
* Handy bookmarklets to easily shorten and share links
* Awesome stats: historical click reports, referrers tracking, visitors geo-location
* Neat Ajaxed interface
* Terrific Plugin architecture to easily implement new features
* Cool developer API
* Full jsonp support
* Friendly installer
* Sample files to create your own public interface and more


