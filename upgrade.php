<?php
// heavily borrowed from /admin/upgrade.php
require_once('/app/code/includes/load-yourls.php');
require_once('/app/code/includes/functions-upgrade.php');
require_once('/app/code/includes/functions-install.php');

if ( !yourls_upgrade_is_needed() ) {
    echo "Already up to date";
} else {
    list($oldver, $oldsql) = yourls_get_current_version_from_sql();

    $newver = YOURLS_VERSION;
    $newsql = YOURLS_DB_VERSION;

    yourls_debug_mode(true);

    for ($step = 1; $step <= 3; ++$step) {
        echo "Step " . $step;
        $upgrade = yourls_upgrade( $step, $oldver, $newver, $oldsql, $newsql );
        echo "";
    }

    echo "Update succeeded";
}

