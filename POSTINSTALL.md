<nosso>

This app is pre-setup with an admin account. The initial credentials are:

**Username**: admin<br/>
**Password**: changeme<br/>

Please change the admin password and email immediately.
See the app [documentation](https://cloudron.io/documentation/apps/yourls) how to do so.

</nosso>

