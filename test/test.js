#!/usr/bin/env node

/* global it, xit, describe, before, after, afterEach */

'use strict';

require('chromedriver');

const execSync = require('child_process').execSync,
    expect = require('expect.js'),
    fs = require('fs'),
    path = require('path'),
    superagent = require('superagent'),
    { Builder, By, until } = require('selenium-webdriver'),
    { Options } = require('selenium-webdriver/chrome');

describe('Application life cycle test', function () {
    this.timeout(0);

    const LOCATION = process.env.LOCATION || 'test';
    const TEST_TIMEOUT = 10000;
    const EXEC_ARGS = { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' };
    const username = process.env.USERNAME;
    const password = process.env.PASSWORD;

    let browser, app;

    before(function () {
        const chromeOptions = new Options().windowSize({ width: 1280, height: 1024 });
        if (process.env.CI) chromeOptions.addArguments('no-sandbox', 'disable-dev-shm-usage', 'headless');
        browser = new Builder().forBrowser('chrome').setChromeOptions(chromeOptions).build();
        if (!fs.existsSync('./screenshots')) fs.mkdirSync('./screenshots');
    });

    after(function () {
        browser.quit();
    });

    afterEach(async function () {
        if (!process.env.CI || !app) return;

        const currentUrl = await browser.getCurrentUrl();
        if (!currentUrl.includes(app.domain)) return;
        expect(this.currentTest.title).to.be.a('string');

        const screenshotData = await browser.takeScreenshot();
        fs.writeFileSync(`./screenshots/${new Date().getTime()}-${this.currentTest.title.replaceAll(' ', '_')}.png`, screenshotData, 'base64');
    });

    async function visible(selector) {
        await browser.wait(until.elementLocated(selector), TEST_TIMEOUT);
        await browser.wait(until.elementIsVisible(browser.findElement(selector)), TEST_TIMEOUT);
    }

    async function adminLogin(username, password) {
        await browser.get('https://' + app.fqdn + '/admin/');
        await visible(By.id('username'));
        await browser.findElement(By.id('username')).sendKeys(username);
        await browser.findElement(By.id('password')).sendKeys(password);
        await browser.findElement(By.id('submit')).click();
        await browser.wait(until.elementLocated(By.id('admin_menu_admin_link')), TEST_TIMEOUT);
    }

    async function logout() {
        await browser.get('https://' + app.fqdn + '/admin/');
        await browser.findElement(By.xpath('//a[@title="Logout"]')).click();
        await browser.sleep(2000);
    }

    async function addUrl() {
        await browser.get('https://' + app.fqdn + '/admin/');
        await visible(By.id('add-url'));
        await browser.findElement(By.id('add-url')).sendKeys('https://cloudron.io');
        await browser.findElement(By.id('add-keyword')).sendKeys('cloudron');
        await browser.findElement(By.id('add-button')).click();
        await browser.sleep(3000);
    }

    async function testUrl() {
        const response = await superagent.get('https://' + app.fqdn + '/cloudron').redirects(0).ok(() => true);
        if (response.statusCode !== 301) throw new Error('did not redirect');

        if (response.headers.location !== 'https://cloudron.io') throw new Error('location is wrong');
    }

    async function checkUrl() {
        await browser.get('https://' + app.fqdn + '/admin/');
        await visible(By.xpath('//a[text()="cloudron"]'));
    }

    function getAppInfo() {
        const inspect = JSON.parse(execSync('cloudron inspect'));
        app = inspect.apps.filter(function (a) { return a.location === LOCATION || a.location === LOCATION + '2'; })[0];
        expect(app).to.be.an('object');
    }

    xit('build app', function () {
        execSync('cloudron build', EXEC_ARGS);
    });

    it('install app', function () {
        execSync('cloudron install --location ' + LOCATION, EXEC_ARGS);
        getAppInfo();
    });
    it('can admin login', adminLogin.bind(null, username, password));
    it('add url', addUrl);
    it('can logout', logout);

    it('can test url', testUrl);

    it('backup app', function () {
        execSync('cloudron backup create --app ' + app.id, EXEC_ARGS);
    });

    it('can admin login', adminLogin.bind(null, username, password));
    it('can check url', checkUrl);
    it('can test url', testUrl);
    it('can logout', logout);

    it('restore app', function () {
        const backups = JSON.parse(execSync('cloudron backup list --raw'));
        execSync('cloudron uninstall --app ' + app.id, EXEC_ARGS);
        execSync('cloudron install --location ' + LOCATION, EXEC_ARGS);
        getAppInfo();
        execSync(`cloudron restore --backup ${backups[0].id} --app ${app.id}`, EXEC_ARGS);
    });

    it('can admin login', adminLogin.bind(null, username, password));
    it('can check url', checkUrl);
    it('can test url', testUrl);
    it('can logout', logout);

    it('move to different location', function () {
        execSync('cloudron configure --location ' + LOCATION + '2 --app ' + app.id, EXEC_ARGS);
        getAppInfo();
    });

    it('can admin login', adminLogin.bind(null, username, password));
    it('can check url', checkUrl);
    it('can test url', testUrl);

    it('uninstall app', function () {
        execSync('cloudron uninstall --app ' + app.id, EXEC_ARGS);
    });

    // No SSO
    it('install app (no sso)', function () {
        execSync('cloudron install --no-sso --location ' + LOCATION, EXEC_ARGS);
    });

    it('can get app information', getAppInfo);
    it('can admin login', adminLogin.bind(null, 'admin', 'changeme'));
    it('add url', addUrl);
    it('can logout', logout);

    it('uninstall app (no sso)', function () {
        execSync('cloudron uninstall --app ' + app.id, EXEC_ARGS);
    });

    // test update
    it('can install app', function () {
        execSync('cloudron install --appstore-id org.yourls.cloudronapp --location ' + LOCATION, EXEC_ARGS);
        getAppInfo();
    });

    it('can admin login', adminLogin.bind(null, username, password));
    it('add url', addUrl);
    it('can logout', logout);

    it('can update', function () {
        execSync('cloudron update --app ' + app.id, EXEC_ARGS);
        getAppInfo();
    });

    it('can admin login', adminLogin.bind(null, username, password));
    it('can check url', checkUrl);
    it('can test url', testUrl);

    it('uninstall app', function () {
        execSync('cloudron uninstall --app ' + app.id, EXEC_ARGS);
    });
});
